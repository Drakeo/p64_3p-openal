#!/bin/bash

# turn on verbose debugging output for parabuild logs.
set -x
# make errors fatal
set -e
# complain about unset env variables
set -u

TOP="$(readlink -f $(dirname "$0"))"

FREEALUT_SOURCE_DIR="freealut"
# e.g. PACKAGE_VERSION='1.1.0'
# find the line, then tease out what's between single quotes
FREEALUT_VERSION="$(expr "$(grep '^PACKAGE_VERSION=' "$TOP/$FREEALUT_SOURCE_DIR/configure")" \
                         : ".*'\(.*\)'")"

OPENAL_SOURCE_DIR="openal-soft"
# e.g.
# SET(LIB_MAJOR_VERSION "1")
# SET(LIB_MINOR_VERSION "12")
# SET(LIB_BUILD_VERSION "854")
parts=()
for part in MAJOR MINOR BUILD
do # find each line, then tease out what's between single quotes
   # append each new version number to parts
   parts[${#parts[*]}]="$(expr "$(grep "^SET(LIB_${part}_VERSION" \
                                       "$TOP/$OPENAL_SOURCE_DIR/CMakeLists.txt")" \
                               : '.*"\(.*\)"')"
done
OPENAL_VERSION="$(
    # rejoin parts with a period (in subshell)
    IFS='.'
    echo "${parts[*]}"
)"

if [ -z "$AUTOBUILD" ] ; then
    exit 1
fi

# load autobuild provided shell functions and variables
# first remap the autobuild env to fix the path for sickwin
if [ "$OSTYPE" = "cygwin" ] ; then
    autobuild="$(cygpath -u $AUTOBUILD)"
else
    autobuild="$AUTOBUILD"
fi

stage="$(pwd)"

source_environment_tempfile="$stage/source_environment.sh"
"$autobuild" source_environment > "$source_environment_tempfile"
. "$source_environment_tempfile"


build=${AUTOBUILD_BUILD_ID:=0}
echo "${OPENAL_VERSION}-${FREEALUT_VERSION}.${build}" > "${stage}/VERSION.txt"

case "$AUTOBUILD_PLATFORM" in
    windows*)

        cmake ../$OPENAL_SOURCE_DIR -G "$AUTOBUILD_WIN_CMAKE_GEN" -DCMAKE_INSTALL_PREFIX=$stage \
            -DCMAKE_C_FLAGS="$LL_BUILD" -DCMAKE_CXX_FLAGS="$LL_BUILD"

        mkdir -p "$stage/lib"

        build_sln "OpenAL.sln" "Release|$AUTOBUILD_WIN_VSPLATFORM" "OpenAL32"
        mv Release "$stage/lib/release"
        
        pushd "$TOP/$FREEALUT_SOURCE_DIR/admin/VisualStudioDotNET"
            build_sln "alut.sln" "Release|$AUTOBUILD_WIN_VSPLATFORM" "alut"

            if [ "$AUTOBUILD_ADDRSIZE" = 32 ]
            then bindir=alut/Release
            else bindir=x64/Release
            fi

            cp -a "$bindir/alut.dll" "$stage/lib/release"
            cp -a "$bindir/alut.lib" "$stage/lib/release"
        popd
    ;;
    linux*)
        cmake_args=(-DCMAKE_C_COMPILER=gcc \
            -DCMAKE_C_FLAGS="-m$AUTOBUILD_ADDRSIZE $LL_BUILD_RELEASE" \
            -DCMAKE_CXX_FLAGS="-m$AUTOBUILD_ADDRSIZE $LL_BUILD_RELEASE")

        mkdir -p openal
        pushd openal
            cmake ../../$OPENAL_SOURCE_DIR "${cmake_args[@]}"
            make
        popd

        mkdir -p "$stage/lib/release"
        cp -P "$stage/openal/libopenal.so" "$stage/lib/release"
        cp -P "$stage/openal/libopenal.so.1" "$stage/lib/release"
        cp "$stage/openal/libopenal.so.$OPENAL_VERSION" "$stage/lib/release"

        mkdir -p freealut
        pushd freealut
            cmake ../../$FREEALUT_SOURCE_DIR "${cmake_args[@]}" \
                -DOPENAL_LIB_DIR="$stage/openal" \
                -DOPENAL_INCLUDE_DIR="$TOP/$OPENAL_SOURCE_DIR/include"
            make
            cp -P libalut.so "$stage/lib/release"
            cp -P libalut.so.0 "$stage/lib/release"
            cp -P libalut.so.0.0.0 "$stage/lib/release"
        popd
    ;;
esac

cp -r "$TOP/$OPENAL_SOURCE_DIR/include" "$stage"
cp -r "$TOP/$FREEALUT_SOURCE_DIR/include" "$stage"

mkdir -p "$stage/LICENSES"
cp "$TOP/$OPENAL_SOURCE_DIR/COPYING" "$stage/LICENSES/openal.txt"


